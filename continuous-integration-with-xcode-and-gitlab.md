# Continuous Integration with Xcode & GitLab -- Jeremy White

Are you an iOS developer? Do you use GitLab? Are you super jealous of your
web-developing colleagues getting to use fancy build tools like [GitLab CI][]?
If so, than this writeup is for you!

OK, now that the cheesy intro is out of the way, let’s get started.

## Requirements

* GitLab 8.0 with CI configured ([installation][install-gitlab])
* Mac OS X El Capitan
* Xcode 7.0.1
* iOS 9

The setup instructions assume you have GitLab 8.0 or newer, because of the CI
integration. You can probably get previous versions working, but you’ll have to
find your own way through the setup & UI.

We will be using the latest versions of Apple’s software, but these
instructions might also work with the previous versions as well.

## Install GitLab CI Multi-Runner

The runner is the process that will actually execute the shell commands that
you are sending to it. Generally, the runner would exist on a machine/server
that is dedicated to the purpose. The runner is usually installed on a machine
separate from the GitLab server.

Most projects that will be integrated with GitLab CI can use a runner installed
on any type machine (Linux, Mac, Windows, etc). Unfortunately, being iOS
developers, our runner has to run on a Mac, since that’s the only environment
that supports Xcode and the Xcode command line tools.

For demonstration & testing purposes, we have installed the runner locally on a
MacBook Pro. In a production environment consider setting up
(or [renting][macrent]) a dedicated Mac Mini or similar device to serve as the
runner.

You should follow [GitLab’s installation docs][runner-osx] to get the runner
installed. (Tip: set it up to use shell - RJH)

### Add ios Tag to Runner (optional)

My colleagues mostly consist of web developers, so I want to make sure that my
Mac OS X runner (my laptop) won’t be running the CI jobs for their JavaScript
projects. To accomplish this, I make use of GitLab CI’s tagging system.

1. Navigate to your GitLab server in your browser
1. Click the Admin Area icon in the upper right
1. Click Continuous Integration in the left sidebar
1. Click Runners in the left sidebar
1. Find your runner in the list, and click the Edit button
1. Add ios to the Tags field
1. Click the Save button

## Project Setup

We assume that you have a repository created on your GitLab instance, and you
have committed & push an iOS project to that repository. If you’d like to start
from scratch, that might be easiest. Let's first create a blank project.

![New XCode project](img/new_xcode_project.png)

## Add the project to GitLab CI

1. Navigate to your GitLab server in your browser
1. Click Continuous Integration in the left sidebar
1. Find your project in the list and click Add project to CI
1. Click the Runners in the left sidebar
1. Click the Enable shared runners button

## Share Your Scheme

1. Open your project in Xcode
1. In the menu bar, click Product > Scheme > Manage Schemes…
1. Find the scheme for your project’s main target and make sure the Shared checkbox is checked
1. Close the dialog window
1. Add the new xcscheme file to your repository

![Share scheme](img/share_scheme.png)

## Install xcpretty

When Xcode dumps it’s output to the console, it is in a format which isn’t
easily human-readable. `xcpretty` is a utility that will transform Xcode’s
output into a format that is easy to understand and browse quickly.

Follow their [installation docs][install-xcpretty] to get xcpretty onto your
system.

## Create .gitlab-ci.yml

1. Open your [favorite editor][xkcd] and create a file called `.gitlab-ci.yml`
1. Paste the contents of the below snippet into your editor and save the file
1. Add the file to your repository
1. Commit your changes and push your repo to GitLab

```yaml
stages:
  - build

build_code:
  stage: build
  script:
    - xcodebuild clean -project TestProject.xcodeproj -scheme TestProject | xcpretty -c && exit ${PIPESTATUS[0]}
    - xcodebuild build -project TestProject.xcodeproj -scheme TestProject | xcpretty -c && exit ${PIPESTATUS[0]}
  tags:
    - ios
```

## Watch the build log

1. Navigate to your GitLab server in your browser
1. Click Continuous Integration in the left sidebar
1. Find your project in the list and click it
1. Click the commit id of the top row to see the build information
1. Click the build id of the top row to see the build output

You should see something similar to the screen below.

![Build log](img/build_log.png)

## Configure Provisioning

At this point, you should have a working CI process, but your builds may be
failing due to a provisioning error. You will have to modify the build script
to include references to your team & provisioning profile.

Check out the following for more info on the [xcodebuild arguments][xcodebuild].

## What next?

When learning something new, I like to make subtle changes to see how/if things
break. Here are some suggestions to aid in your learning on this topic.

* try removing xcpretty and see what it looks like
* try un-sharing your Xcode project scheme and see what happens
* check out xcpretty’s options and tweak the output
* check out the different flags for [GitLab CI’s YAML][yaml] config file

There are lots of different possibilities with GitLab CI. Some topics I may
explore in the future include:

* static analysis & linting
* adding a test phase for unit & UI tests
* continuous deployment

[GitLab CI]: https://about.gitlab.com/gitlab-ci/
[install-gitlab]: https://about.gitlab.com/downloads/
[macrent]: https://macstadium.com/
[runner-osx]: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md
[install-xcpretty]: https://github.com/supermarin/xcpretty
[xkcd]: https://xkcd.com/378/
[xcodebuild]: https://coderwall.com/p/rv2lgw/use-xcodebuild-to-build-workspace-vs-project
[yaml]: http://doc.gitlab.com/ci/yaml/README.html
